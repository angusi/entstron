package net.entscrew.entstron.java.ui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import net.entscrew.entstron.java.Entstron;
import net.entscrew.entstron.java.devices.Yamaha.MTX.Preset;
import net.entscrew.entstron.java.devices.Yamaha.MTX.YamahaMTX;

public class MTXView extends AbstractView {
    public TilePane presetButtons;
    private ChangeListener<Number> presetButtonChangeListener;
    private YamahaMTX mtx;

    @Override
    public void onLoad() {
        mtx = (YamahaMTX) Entstron.getDeviceInstance(YamahaMTX.class);

        presetButtonChangeListener = (observableValue, oldValue, newValue) ->
                Platform.runLater(this::updatePresetButtons);
    }

    @Override
    public void onHide() {
        mtx.currentPresetProperty().removeListener(presetButtonChangeListener);
    }

    @Override
    public void onShow() {
        mtx.currentPresetProperty().addListener(presetButtonChangeListener);

        updatePresetButtons();
    }

    private void updatePresetButtons() {
        presetButtons.getChildren().clear();
        mtx.getPresetsInfo().stream().filter(preset -> preset.getAttrib() == Preset.ATTRIB.USER).forEach(preset -> {
            Button newButton = new Button(preset.getTitleText());
            newButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            newButton.setOnAction(actionEvent -> mtx.setPreset(preset.getPresetNumber()));
            if(Integer.parseInt(preset.getPresetNumber()) == mtx.getCurrentPreset()) {
                newButton.getStyleClass().add("active");
            }
            presetButtons.getChildren().add(newButton);
        });
    }
}
