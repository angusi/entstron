package net.entscrew.entstron.java.ui;

public abstract class AbstractView {

    protected NodeInstance nodeInstance;

    public void onLoad() {}
    public void onHide() {}
    public void onShow() {}
}
