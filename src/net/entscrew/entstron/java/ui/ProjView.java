package net.entscrew.entstron.java.ui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import net.entscrew.entstron.java.Entstron;
import net.entscrew.entstron.java.devices.NEC.Projectors.NECProjector;

import java.util.ArrayList;
import java.util.List;

public class ProjView extends AbstractView {
    public TilePane projectorButtons;
    private List<NECProjector> projectors;
    private ChangeListener<NECProjector.POWER_STATUS> projectorButtonChangeListener;

    @Override
    public void onLoad() {
        projectors = new ArrayList<>();
        projectors.add((NECProjector) Entstron.getDeviceInstance(NECProjector.class));

         projectorButtonChangeListener = (observableValue, oldValue, newValue) ->
                Platform.runLater(this::updateProjectorButtons);
    }

    @Override
    public void onHide() {
        projectors.forEach(proj -> proj.powerStatusProperty().removeListener(projectorButtonChangeListener));
    }

    @Override
    public void onShow() {
        projectors.forEach(proj -> proj.powerStatusProperty().addListener(projectorButtonChangeListener));

        updateProjectorButtons();
    }

    private void updateProjectorButtons() {
        projectorButtons.getChildren().clear();

        projectors.forEach(projector -> {
            Button newButton = new Button("PROJECTOR!"); //TODO: Proj name?
            newButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            newButton.setOnAction(actionEvent -> projector.requestTogglePowerStatus());
            switch(projector.getPowerStatus()) {
                case OFFLINE:
                    newButton.getStyleClass().add("offline");
                    break;
                case OFF:
                    newButton.getStyleClass().add("off");
                    break;
                case ON:
                    newButton.getStyleClass().add("on");
                    break;
            }
            projectorButtons.getChildren().add(newButton);
        });
    }
}
