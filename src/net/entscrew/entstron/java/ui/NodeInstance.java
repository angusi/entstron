package net.entscrew.entstron.java.ui;

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.stage.Screen;
import net.entscrew.entstron.java.Entstron;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeInstance {
    private static Map<Class<? extends AbstractView>, NodeInstance> classHandles = new HashMap<>();
    private static Logger logger;
    private Node node;
    private AbstractView viewController;


    private NodeInstance(Class<? extends AbstractView> handle) {
        logger = LoggerFactory.getLogger(handle);

        logger.trace("Setting up " + handle.getSimpleName() + " scene");

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(handle.getResource("/fxml/"+handle.getSimpleName()+".fxml"), Entstron.getMessages());
            this.node = fxmlLoader.load();
            viewController= fxmlLoader.getController();
            viewController.nodeInstance = this;

            viewController.onLoad();

        } catch (IOException e) {
            logger.error("Failed to load Scene for "+handle.getClass().getSimpleName()+".");
            e.printStackTrace();
        }
    }

    public Node getNode() {
        return node;
    }

    public static NodeInstance getHandle(Class<? extends AbstractView> scene) {
        NodeInstance classHandle = classHandles.get(scene);
        if(classHandle == null) {
            classHandle = new NodeInstance(scene);
            classHandles.put(scene, classHandle);
        }
        return classHandle;
    }

    public AbstractView getViewController() {
        return viewController;
    }
}
