package net.entscrew.entstron.java.ui;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import net.entscrew.entstron.java.Entstron;
import net.entscrew.entstron.java.devices.AbstractNetworkDevice;
import net.entscrew.entstron.java.devices.Yamaha.MTX.YamahaMTX;

public class MainView extends AbstractView {
    public Button mtxButton;
    public TilePane mainPane;
    public Button projButton;
    private Thread updateButtonLoop;

    public void mtxButtonAction(ActionEvent actionEvent) {
        Entstron.getHandle().changeNode(MTXView.class);
    }

    @Override
    public void onHide() {
        updateButtonLoop.interrupt();
    }

    @Override
    public void onShow() {
        //Update button statuses
        updateButtonLoop = new Thread(this::updateButtonLoop);
        updateButtonLoop.start();
    }

    private void updateButtonLoop() {
        YamahaMTX mtx = (YamahaMTX) Entstron.getDeviceInstance(YamahaMTX.class);
        while (true) {
            //Yamaha MTX:
            if (mtx != null) {
                AbstractNetworkDevice.STATUSES mtxStatus = mtx.getStatus();
                mtxButton.getStyleClass().removeAll("inactive", "error", "warning", "normal");
                switch (mtxStatus) {
                    case UNKNOWN:
                        mtxButton.getStyleClass().add("inactive");
                        break;
                    case ERROR:
                        mtxButton.getStyleClass().add("error");
                        break;
                    case WARNING:
                        mtxButton.getStyleClass().add("warning");
                        break;
                    case OK:
                        mtxButton.getStyleClass().add("normal");
                        break;
                }
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public void projButtonAction(ActionEvent actionEvent) {
        Entstron.getHandle().changeNode(ProjView.class);
    }

    public void terminateApplication(ActionEvent actionEvent) {
        Entstron.getHandle().quitApplication();
    }
}
