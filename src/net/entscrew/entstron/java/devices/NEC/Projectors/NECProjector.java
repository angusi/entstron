package net.entscrew.entstron.java.devices.NEC.Projectors;

import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableObjectValue;
import net.entscrew.entstron.java.devices.AbstractNetworkDevice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class NECProjector extends AbstractNetworkDevice {

    /**
     * Default connection port
     */
    public static final int DEFAULT_PORT = 7142;

    public enum POWER_STATUS { ON, OFF, OFFLINE }
    private ObjectProperty<POWER_STATUS> powerStatus = new SimpleObjectProperty<>(POWER_STATUS.OFFLINE);

    /**
     * Instantiate a new instance of the Remote Controller.
     * @param projectorServerSocket An InetSocketAddress instance with the IP and port that the MTX listens on
     */
    public NECProjector(InetSocketAddress projectorServerSocket) {
        super(projectorServerSocket);
        requestStatusInformation();
    }

    private void requestStatusInformation() {
        //TODO: Switch to BASIC INFORMATION?
        byte[] data = {0x00, (byte) 0x85, 0x00, 0x00, (byte)0x01, (byte)0x01, (byte) 0x87};
        sendData(data);
    }

    public void requestTogglePowerStatus() {
        byte[] data = null;
        if(getPowerStatus() == POWER_STATUS.OFF || getPowerStatus() == POWER_STATUS.OFFLINE) {
            data = new byte[]{(byte)0x02, 0x00, 0x00, 0x00, 0x00, (byte)0x02};
        } else if(getPowerStatus() == POWER_STATUS.ON) {
            data = new byte[]{(byte)0x02, (byte)0x01, 0x00, 0x00, 0x00, (byte)0x03};
        }
        sendData(data);
    }

    private void sendData(byte[] data) {
        logger.info("Sending "+ Arrays.toString(data));
        if(socketChannel != null && socketChannel.isConnected()) {
            ByteBuffer bytes = ByteBuffer.wrap(data);
            try {
                while (bytes.hasRemaining()) {
                    socketChannel.write(bytes);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } //TODO: else { ... }
    }

    @Override
    protected void handleReceivedData(ByteBuffer dataBuffer) throws UnsupportedEncodingException {
        logger.info("Received "+ Arrays.toString(dataBuffer.array()));
        byte[] bytes = dataBuffer.array();
        if((bytes[0] == (byte)0x20 || bytes[0] == (byte)0xA0) && bytes[1] == (byte)0x85) {
            receivedRunStatus(bytes);
        } else if((bytes[0]==(byte)0x22 || bytes[0]==(byte)0xA2) && bytes[1] == (byte)0x00) {
            receivedPowerOnConfirmation(bytes);
        }  else if((bytes[0]==(byte)0x22 || bytes[0]==(byte)0xA2) && bytes[1] == (byte)0x01) {
            receivedPowerOffConfirmation(bytes);
        }
    }

    private void receivedPowerOffConfirmation(byte[] bytes) {
        logger.info("Projector power off: {}", (bytes[0] == (byte)0x22 ? "SUCCESS" : "FAILURE"));
        powerStatus.setValue(POWER_STATUS.OFF);
    }

    private void receivedPowerOnConfirmation(byte[] bytes) {
        logger.info("Projector power on: {}", (bytes[0] == (byte)0x22 ? "SUCCESS" : "FAILURE"));
        powerStatus.setValue(POWER_STATUS.ON);
    }

    private void receivedRunStatus(byte[] bytes) {
        if(bytes[7] == 0x00) {
            logger.info("Project is OFF");
            powerStatus.setValue(POWER_STATUS.OFF);
        } else if(bytes[7] == 0x01) {
            logger.info("Project is ON");
            powerStatus.setValue(POWER_STATUS.ON);
        }
    }

    public POWER_STATUS getPowerStatus() {
        return powerStatus.get();
    }

    public ObjectProperty<POWER_STATUS> powerStatusProperty() {
        return powerStatus;
    }
}
