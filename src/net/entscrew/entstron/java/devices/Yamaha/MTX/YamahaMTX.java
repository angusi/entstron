package net.entscrew.entstron.java.devices.Yamaha.MTX;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import net.entscrew.entstron.java.devices.AbstractNetworkDevice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Yamaha MTX Series Matrix Processor device
 *
 * This class is implemented against Version 2.1.0 rev7 of the control specifications
 * provided by Yamaha online at
 * http://download.yamaha.com/api/asset/file?language=en&site=countrysite-master.prod.wsys.yamaha.com&asset_id=63959
 *
 * This class is implemented against a MTX5-D Series Matrix Processor and is not guaranteed against any other model.
 */
public class YamahaMTX extends AbstractNetworkDevice {

    /**
     * Default connection port
     */
    public static final int DEFAULT_PORT = 49280;

    /**
     * Instantiate a new instance of the Remote Controller.
     * @param mtxServerSocket An InetSocketAddress instance with the IP and port that the MTX listens on
     */
    public YamahaMTX(InetSocketAddress mtxServerSocket) {
        super(mtxServerSocket);
        requestStatusInformation();
        requestPresets();
    }

    private Map<Integer, Preset> presetList = new HashMap<>();
    private IntegerProperty numPresets;
    private IntegerProperty currentPreset = new SimpleIntegerProperty();

    /**
     * Handle data received from the MTX. Dispatches processing of specific data to specific methods.
     * @param buffer Up to 1024 bytes of data read from the MTX
     * @throws UnsupportedEncodingException If ASCII isn't available(?) for decoding the data.
     */
    protected void handleReceivedData(ByteBuffer buffer) throws UnsupportedEncodingException {
        String data = new String(buffer.array(), buffer.position(), buffer.limit(), "ASCII");
        logger.info("Received: "+data);
        StringTokenizer messageTokenizer = new StringTokenizer(data, "\n");
        while(messageTokenizer.hasMoreTokens()) {
            String message = messageTokenizer.nextToken();
            logger.info("Parsing "+message);
            StringTokenizer tokenizer = new StringTokenizer(message);
            String status = tokenizer.nextToken();
            if (!status.equals("OK") && !status.equals("NOTIFY")) {
                logger.warn("STATUS NOT OK!\n{}", data);
            }

            String command = tokenizer.nextToken();
            switch (command) {
                case "devstatus":
                    receivedDevstatus(tokenizer);
                    break;
                case "ssnum":
                    receivedNumberOfPresets(tokenizer);
                    break;
                case "ssinfo":
                    receivedPresetInformation(tokenizer);
                    break;
                case "sscurrent":
                case "ssrecall":
                    receivedPresetNumberUpdate(tokenizer);
                    break;
                default:
                    logger.warn("Unknown message, ignoring - " + command);
            }
        }
    }

    private void receivedPresetNumberUpdate(StringTokenizer tokenizer) {
        int newPresetNumber = Integer.parseInt(tokenizer.nextToken());
        logger.info("New preset: {}",newPresetNumber);
        currentPreset.setValue(newPresetNumber);
    }

    private void receivedPresetInformation(StringTokenizer tokenizer) {
        Preset preset = new Preset();

        Integer number = Integer.parseInt(tokenizer.nextToken());

        StringBuilder presetNumber = new StringBuilder();
        while(tokenizer.hasMoreTokens()) {
            String nextToken = tokenizer.nextToken();
            presetNumber.append(nextToken);
            if(nextToken.endsWith("\"")) {
                break;
            }
        }
        preset.setPresetNumber(presetNumber.toString().substring(1, presetNumber.length()-1));

        preset.setAttrib(Preset.ATTRIB.valueOf(tokenizer.nextToken().toUpperCase()));

        StringBuilder presetTitle = new StringBuilder();
        while(tokenizer.hasMoreTokens()) {
            String nextToken = tokenizer.nextToken();
            presetTitle.append(nextToken);
            if(nextToken.endsWith("\"")) {
                break;
            }
        }
        preset.setTitleText(presetTitle.toString().substring(1, presetTitle.length()-1));

        StringBuilder presetComment = new StringBuilder();
        while(tokenizer.hasMoreTokens()) {
            String nextToken = tokenizer.nextToken();
            presetComment.append(nextToken);
            if(nextToken.endsWith("\"")) {
                break;
            }
        }
        preset.setComment(presetComment.toString().substring(1, presetComment.length()-1));

        presetList.put(number, preset);
    }

    private void receivedNumberOfPresets(StringTokenizer tokenizer) {
        if(numPresets == null) {
            numPresets = new SimpleIntegerProperty();
            numPresets.addListener(observable -> requestPresets());
        }
        numPresets.setValue(Integer.parseInt(tokenizer.nextToken()));
    }

    /**
     * Process a device status update, or devstatus, message.
     * @param tokenizer The tokenizer instance containing this message.
     */
    private void receivedDevstatus(StringTokenizer tokenizer) {
        logger.info("Received devstatus.");
        switch(tokenizer.nextToken()) {
            case "runmode":
                logger.trace("Runmode: "+tokenizer.nextToken());
                //TODO: So... what?
                break;
            case "error":
                logger.trace("Error: ");
                String errorString = tokenizer.nextToken();
                if(errorString.equals("\"none\"")) {
                    setStatus(STATUSES.OK);
                } else if(errorString.startsWith("\"flt")) {
                    setStatus(STATUSES.ERROR);
                    logger.info("System fault - " + errorString);
                } else if(errorString.startsWith("\"err")) {
                    setStatus(STATUSES.ERROR);
                    logger.info("System error - " + errorString);
                } else if(errorString.startsWith("\"wrn")) {
                    setStatus(STATUSES.WARNING);
                    logger.info("System warning - " + errorString);
                } else {
                    setStatus(STATUSES.UNKNOWN);
                    logger.warn("Unknown error status - " + errorString);
                }
        }
    }

    private void sendData(String data) {
        logger.info("Sending "+data);
        if(socketChannel != null && socketChannel.isConnected()) {
            ByteBuffer bytes = ByteBuffer.wrap(data.getBytes(Charset.forName("ASCII")));
            try {
                while (bytes.hasRemaining()) {
                    socketChannel.write(bytes);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } //TODO: else { ... }
    }

    /**
     * Request a device status, or devstatus runmode, update.
     */
    public void requestStatusInformation() {
        sendData("devstatus runmode\n");
        sendData("devstatus error\n");
        sendData("sscurrent\n");

    }

    public void requestNumberOfPresets() {
        sendData("ssnum\n");
    }

    public void requestPresets() {
        if(numPresets == null) {
            requestNumberOfPresets();
            return;
        }

        for(int i = 0; i < numPresets.getValue(); i++ ){
            sendData("ssinfo "+i+"\n");
        }
    }

    public int getCurrentPreset() {
        return currentPreset.get();
    }

    public IntegerProperty currentPresetProperty() {
        return currentPreset;
    }

    public Preset getPresetInfo(int presetNumber) {
        return presetList.get(presetNumber);
    }

    public List<Preset> getPresetsInfo() {
        return presetList.entrySet()
                .stream()
                .sorted((p1, p2) -> p1.getKey().compareTo(p2.getKey()))
                .map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public int getNumPresets() {
        return numPresets.get();
    }

    public IntegerProperty numPresetsProperty() {
        return numPresets;
    }

    public void setPreset(String presetNumber) {
        try {
            sendData("ssrecall " + Integer.parseInt(presetNumber) + " \n");
        } catch (NumberFormatException e) {
            logger.error("Tried to recall a preset but it wasn't a number!");
        }
    }
}
