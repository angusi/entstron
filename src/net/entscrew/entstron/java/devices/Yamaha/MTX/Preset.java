package net.entscrew.entstron.java.devices.Yamaha.MTX;

public class Preset {

    public enum ATTRIB { PREINST, RESERVE, USER, EMPTY;}

    String presetNumber;
    ATTRIB attrib;
    String titleText;
    String comment;

    public String getPresetNumber() {
        return presetNumber;
    }

    public void setPresetNumber(String presetNumber) {
        this.presetNumber = presetNumber;
    }

    public ATTRIB getAttrib() {
        return attrib;
    }

    public void setAttrib(ATTRIB attrib) {
        this.attrib = attrib;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
