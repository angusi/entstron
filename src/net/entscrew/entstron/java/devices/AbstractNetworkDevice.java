package net.entscrew.entstron.java.devices;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Instances of remotely controllable networked devices extend from this class.
 *
 * This class provides the core functionality of Networked Devices.
 * It includes threading code to keep network I/O operations off the main and UI threads
 *
 * @author Angus Ireland
 */
public abstract class AbstractNetworkDevice implements Runnable {
    /**
     * Device network connection information
     */
    @NotNull private final InetSocketAddress networkServerAddress;

    /**
     * Logger instance
     */
    protected final Logger logger;

    /**
     * Network connection to device
     */
    @Nullable protected SocketChannel socketChannel;

    /**
     * Abstract status information.
     * Can apply to any device, devices may choose to
     * implement a more detailed status report function.
     */
    public enum STATUSES { OK, WARNING, ERROR, UNKNOWN }

    /**
     * Abstract status information.
     * Can apply to any device, devices may choose to
     * implement a more detailed status report function.
     */
    @NotNull private STATUSES status = STATUSES.UNKNOWN;

    @NotNull protected String messageSeparator;

    /**
     * Instantiate a new AbstractNetworkDevice object.
     * Automatically makes a single connection attempt to the device.
     * @param networkServerAddress An InetSocketAddress instance with the IP and port that the device listens on.
     */
    protected AbstractNetworkDevice(@NotNull InetSocketAddress networkServerAddress) {
        logger = LoggerFactory.getLogger(this.getClass());

        this.networkServerAddress = networkServerAddress;
        try {
            socketChannel = connectSocket(networkServerAddress);
            logger.info("Connected to "+networkServerAddress.toString());
        } catch (IOException e) {
            logger.error("Could not connect to "+networkServerAddress.toString()+" - "+e.getMessage());
        }
    }

    /**
     * Attempt to make a connection to the device
     * @param networkServerAddress An InetSocketAddress instance with the IP and port that the device listens on.
     * @return An instance of SocketChannel, if the connection was successful
     * @throws IOException If the connection could not be made, an IOException is raised. See SocketChannel doc.
     */
    @NotNull
    private static SocketChannel connectSocket(InetSocketAddress networkServerAddress) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(networkServerAddress);
        return socketChannel;
    }

    /**
     * Incoming data loop.
     * Hands off reading of data to #handleReceivedData
     * On connection loss, attempts reconnection.
     */
    @Override
    public void run() {
        while(true) {
            try {
                ByteBuffer dataBuffer = ByteBuffer.allocate(65535);
                try {
                    while (socketChannel != null && socketChannel.isConnected()) {
                        int bytesRead = socketChannel.read(dataBuffer);
                        if (bytesRead > 0) {
                            //TODO: Perhaps read until separator?
                            dataBuffer.flip();
                            handleReceivedData(dataBuffer);
                            dataBuffer.clear();
                        }
                        Thread.sleep(100); //TODO: Why 100ms? Is a pause even necessary? Or do we want to prevent busy-spin?
                    }
                } catch (IOException e) {
                    logger.info("Connection to {} closed or busy-spin wait interrupted.", networkServerAddress.toString());
                }

                //Socket must have disconnected :(
                try {
                    if (socketChannel != null) {
                        socketChannel.close();
                    }
                    socketChannel = connectSocket(networkServerAddress);
                } catch (IOException e) {
                    logger.error("Could not connect to " + networkServerAddress.toString() + " - " + e.getMessage());
                    //TODO: Some kind of exponential backoff?
                    Thread.sleep(100);
                }
            } catch(InterruptedException e) {
                try {
                    socketChannel.close();
                } catch (IOException e1) {
                    logger.error("IO Exception when closing socket channel");
                }
                return;
            }
        }
    }

    /**
     * Handle data received from a network device
     * @param dataBuffer Up to 1024 bytes of data read from the networked device.
     * @throws UnsupportedEncodingException Device may want to parse the bytes as text. If the system fails to support
     * the encoding used for this text, this will be thrown.
     */
    protected abstract void handleReceivedData(ByteBuffer dataBuffer) throws UnsupportedEncodingException;

    /**
     * Get the abstract status of the device.
     * @return A member of the STATUSES enum describing general device status
     */
    @NotNull
    public STATUSES getStatus() {
        return status;
    }

    /**
     * Update the abstract status of the device.
     * @param status A member of the STATUSES enum describing general device status
     */
    protected void setStatus(@NotNull STATUSES status) {
        this.status = status;
    }
}
