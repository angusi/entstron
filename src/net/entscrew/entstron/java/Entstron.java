package net.entscrew.entstron.java;


import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import net.entscrew.entstron.java.devices.AbstractNetworkDevice;
import net.entscrew.entstron.java.devices.NEC.Projectors.NECProjector;
import net.entscrew.entstron.java.devices.Yamaha.MTX.YamahaMTX;
import net.entscrew.entstron.java.ui.AbstractView;
import net.entscrew.entstron.java.ui.MainView;
import net.entscrew.entstron.java.ui.NodeInstance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Entstron - Extensible, customisable, touch-friendly, Networked AV Controller
 *
 * The Entstron is a replacement for big-name touch panel control
 * software that typically requires specialised software for reprogramming.
 *
 * It is extensible in that a programmer may add extra device control code
 * without specialist software - aside from a text-editor and a compiler, of course.
 *
 * Customisable in that devices can be added to and removed from the system
 * by the operators of the system, possibly even at runtime!
 *
 * Touch-friendly interfaces are provided for most (all???) functions.
 *
 * @author Angus Ireland
 */
public class Entstron extends Application {

    private static Entstron handle;

    /**
     * Messages bundle with language strings
     */
    private static ResourceBundle messages;

    /**
     * List of devices connected to the Entstron controller network
     */
    private static final Map<Class<?>, AbstractNetworkDevice> devices = new HashMap<>();

    /**
     * List of threads that Entstron device controllers are running on.
     */
    private static final Map<AbstractNetworkDevice, Thread> deviceThreads = new HashMap<>();

    /**
     * Logger instance
     */
    private static Logger logger;

    private BorderPane border;

    private NodeInstance currentNode;

    /**
     * Entstron Entry point
     * @param args No arguments expected
     */
    public static void main(String[] args) {
        Locale currentLocale = new Locale("en", "GB");
        messages = ResourceBundle.getBundle("Messages", currentLocale);
        logger = LoggerFactory.getLogger(Entstron.class);


        logger.info(getAppTitle()+" version "+getAppVersion());

        if(args.length<1) {
            logger.error("Please specify connections file as first argument.");
            System.exit(-1);
        }

//        initialiseDevices(args[0]);


//        //TODO: Load device connections
        logger.trace("Initialising hard-coded network connections");
        logger.warn("WARNING - HARD-CODED CONNECTIONS ARE A PRE-RELEASE FEATURE.");

        InetSocketAddress mtxSocketAddress = new InetSocketAddress("10.1.10.3", YamahaMTX.DEFAULT_PORT);
        //InetSocketAddress mtxSocketAddress = new InetSocketAddress("127.0.0.1", YamahaMTX.DEFAULT_PORT);
        YamahaMTX mtx = new YamahaMTX(mtxSocketAddress);
        devices.put(YamahaMTX.class, mtx);
        Thread mtxThread = new Thread(mtx);
        mtxThread.setName("MTX "+mtxSocketAddress.toString());
        deviceThreads.put(mtx, mtxThread);
        mtxThread.start();

        InetSocketAddress projSocketAddress = new InetSocketAddress("10.1.10.70", NECProjector.DEFAULT_PORT);
//        InetSocketAddress projSocketAddress = new InetSocketAddress("127.0.0.1", NECProjector.DEFAULT_PORT);
        NECProjector proj = new NECProjector(projSocketAddress);
        devices.put(NECProjector.class, proj);
        Thread projThread = new Thread(proj);
        projThread.setName("Proj "+projSocketAddress.toString());
        deviceThreads.put(proj, projThread);
        projThread.start();

        logger.trace("Initialising JavaFX");
        launch(args);
    }

    /**
     * Gets the (localised) application name
     * @return Application name string
     */
    @NotNull
    public static String getAppTitle() {
        return messages.getString("APPTITLE");
    }

    /**
     * Gets the version identifier for this build of the Entstron software
     * @return Version ID string
     */
    @NotNull
    public static String getAppVersion() {
        return messages.getString("APPVERSION");
    }

    private static void initialiseDevices(String connectionsFile) {
        BufferedReader deviceReader = null;
        try {
            deviceReader = new BufferedReader(new FileReader(connectionsFile));
        } catch (FileNotFoundException e) {
            logger.error("Please specify connections file as first argument.");
            System.exit(-1);
        }

        String[] devices = (String[]) deviceReader.lines().toArray();
        for(String deviceString : devices) {
            String[] params = deviceString.split(",");

            if(params.length != 3) {
                logger.error("Incorrect amount of information specified for device.");
                continue;
            }

            Class<?> deviceClass = null;
            Constructor<?> deviceConstructor = null;
            try {
                deviceClass = Class.forName(params[0]);
                deviceConstructor = deviceClass.getConstructor(InetSocketAddress.class);
            } catch (ClassNotFoundException e) {
                logger.error("Unknown device \""+params[0]+"\"");
                continue;
            } catch (NoSuchMethodException e) {
                logger.error("Internal error locating device class");
                continue;
            }

            InetSocketAddress deviceAddress = new InetSocketAddress(params[1], Integer.parseInt(params[2]));
            try {
                AbstractNetworkDevice deviceInstance = (AbstractNetworkDevice) deviceConstructor.newInstance(deviceAddress);
                Entstron.devices.put(deviceClass, deviceInstance);
                Thread deviceThread = new Thread(deviceInstance);
                Entstron.deviceThreads.put(deviceInstance, deviceThread);
                deviceThread.start();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * Cleanly terminates the application.
     */
    public void quitApplication() {
        logger.info("Terminating.");
        this.currentNode.getViewController().onHide();
        deviceThreads.values().forEach(thread -> {
            try {
                thread.interrupt();
                thread.join();
            } catch (InterruptedException e) {
                logger.error("Interrupted while waiting for thread "+thread.getName()+" to terminate.");
            }
        });
        System.exit(0);
    }

    /**
     * Launches the JavaFX User Interface (called by JavaFX)
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        handle = this;

        Stage stage = new Stage();
        stage.setTitle(Entstron.getAppTitle());
        stage.setResizable(false);
        stage.sizeToScene();
        stage.centerOnScreen();

        stage.setOnCloseRequest(windowEvent -> quitApplication());

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        border = new BorderPane();
        border.setPrefSize(primaryScreenBounds.getWidth(), primaryScreenBounds.getHeight());

        Scene scene = new Scene(border);
        stage.setX(primaryScreenBounds.getMinX());
        stage.setY(primaryScreenBounds.getMinY());
        stage.setWidth(primaryScreenBounds.getWidth());
        stage.setHeight(primaryScreenBounds.getHeight());
        stage.setScene(scene);

        changeNode(MainView.class);

        stage.show();
    }

    public void changeNode(Class <? extends AbstractView> NewNodeController) {
        NodeInstance oldNode = this.currentNode;
        NodeInstance newNode = NodeInstance.getHandle(NewNodeController);
        this.currentNode = NodeInstance.getHandle(MainView.class);

        if(oldNode != null) {
            oldNode.getViewController().onHide();
        }
        border.setCenter(newNode.getNode());
        newNode.getViewController().onShow();
    }

    /**
     * Gets the messages bundle for localisation
     * @return A ResourceBundle instance for the user's localisation
     */
    public static ResourceBundle getMessages() {
        return messages;
    }

    /**
     * Gets the instance of the remote controller.
     * Note this interface is subject to change during development until
     * loading of device configuration is complete
     * @param device The class of device to load
     * @return An instance of AbstractNetworkDevice, or null the device has no know connections.
     */
    @Nullable
    public static AbstractNetworkDevice getDeviceInstance(Class<? extends AbstractNetworkDevice> device) {
        return devices.get(device);
    }

    public static Entstron getHandle() {
        return handle;
    }
}
