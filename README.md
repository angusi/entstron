# Entstron #

*Entstron - Extensible, customisable, touch-friendly, Networked AV Controller*

The Entstron is a replacement for big-name touch panel control software that typically requires specialised software for reprogramming.

It is extensible in that a programmer may add extra device control code without specialist software - aside from a text-editor and a compiler, of course.

Customisable in that devices can be added to and removed from the system by the operators of the system, possibly even at runtime!

Touch-friendly interfaces are provided for most (all???) functions.

### Running Entstron ###

For now, you must build Entstron from source.

Follow the instructions in **Building Entstron**, then run

    $ gradle run

### Building Entstron ###

Entstron makes use of the Gradle build system, which makes it super easy to build.
Gradle run on Mac, Windows, and Linux so grab a copy - from your package manager or https://gradle.org - and install that first.

Then, clone this repo and cd into the root. Run:

    $ gradle build